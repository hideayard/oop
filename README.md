# Formulatrix Library
This Project is a simple project for example of Object Oriented Programming in C# 

In this example project, we only use 1 file.

  - Program.cs

# Functions
There are 4 main functions on Program.cs file : 
  - Register
  - Retrieve
  - getType
  - Deregister

### Detail Functions - Input parameters

Dillinger uses a number of open source projects to work properly:

* [Register] - should consist of 3 parameters, (String itemName, String itemContent, int itemType ))
* [Retrieve] - the input parameter should be (String itemName )
* [GetType] - the input parameter should be (String itemName )
* [Deregister] - the input parameter should be (String itemName )

### Detail Functions - Output parameters

* [Register] - return void (no return), just console to log the process
* [Retrieve] - return string (the content of selected filename)
* [GetType] - return integer (1 is JSON filetype, 2 is XML filetype, 0 is not found)
* [Deregister] - return void (no return), just console to log the process

### Testing 
for testing : 
- open project on vscode
- open new terminal on vscode (ctrl+shift+`) 
- run
```sh
dotnet build
dotnet run
```
