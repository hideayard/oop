using System;
using System.Collections;
using System.IO;

namespace FT
{
    class Formulatrix
{
    ArrayList dbName = new ArrayList(),dbContent = new ArrayList(),dbType = new ArrayList();
    //we are creating 3 arraylist for name, content, type
    // Store an item to the repository.
    // Parameter itemType is used to differentiate JSON or XML.
    // 1 = itemContent is a JSON string.
    // 2 = itemContent is an XML string.
public void Register( String itemName, String itemContent, int itemType )
{   // check the extension from path / string file (json or not).
    if(Path.GetExtension(itemName) == ".json" && itemType==1)
    {   // check the file is valid JSON or not
        Console.WriteLine("isValidJSON = "+isValidJSON(itemContent));
        if(isValidJSON(itemContent))
        {
            dbName.Add(itemName);  //add itemName to dbName arraylist
            dbContent.Add(itemContent);  //add itemContent to dbContent arraylist
            dbType.Add(itemType);  //add itemType to dbType arraylist
            //shows notification, success add to DB
            Console.WriteLine("Registration of "+itemName +" Success!");
        }
        else
        {   //shows notification the file is not valid JSON format
            Console.WriteLine(itemName+" is not valid JSON");
        }
    }
    else if(Path.GetExtension(itemName) == ".xml" && itemType==2)
    {
        // check the file is valid JSON or not
        Console.WriteLine("isValidXML = "+isValidXML(itemContent));
        if(isValidXML(itemContent))
        {
            dbName.Add(itemName);  //add itemName to dbName arraylist
            dbContent.Add(itemContent);  //add itemContent to dbContent arraylist
            dbType.Add(itemType);  //add itemType to dbType arraylist
            //shows notification, success add to DB
            Console.WriteLine("Registration of "+itemName +" Success!");
        }
        else
        {   //shows notification the file is not valid XML format
            Console.WriteLine(itemName+" is not valid XML");
        }
    }
    else
    {   //shows notification the file is neither JSON or XML 
        Console.WriteLine(itemName+" is neither JSON or XML (or wrong itemType)");
    }
   
}

// Retrieve an item from the repository.
public String Retrieve( String itemName )
{   //check if selected item present on arraylist
    if(dbName.Contains(itemName))
    {   //show info about content of selected document
        Console.WriteLine("Retrieving "+itemName+" data = "+dbContent[dbName.IndexOf(itemName)]);
        return (String) dbContent[dbName.IndexOf(itemName)];
    }
    else
    {   //show info that selected file not found on database
        Console.WriteLine(itemName+ " not found.");
        //return "" when not found
        return itemName+ " not found.";
    }
    
}
// Retrieve the type of the item (JSON or XML).
public int GetType( string itemName )
{   //check if selected item present on arraylist
    if(dbName.Contains(itemName))
    {   //show info about type of selected document
        Console.WriteLine("Retrieving "+itemName+" dataType = "+dbType[dbName.IndexOf(itemName)]);
        //return data from arraylist (type)
        return (Int32) dbType[dbName.IndexOf(itemName)];
    }
    else
    {   //show info that selected file not found on database
        Console.WriteLine(itemName +" not found.");
        //return 0 when not found
        return 0;
    }
}
// Remove an item from the repository.
public void Deregister( string itemName )
{   //check if selected item present on arraylist
    if(dbName.Contains(itemName))
    {   //remove selected item from arraylist
        dbContent.RemoveAt(dbName.IndexOf(itemName));
        dbType.RemoveAt(dbName.IndexOf(itemName));
        //because of itemName is used for index search, so it must be delete at last
        dbName.RemoveAt(dbName.IndexOf(itemName));
        //print info after delete success
        Console.WriteLine("Removing "+itemName+" from Database");
    }
    else
    {   // show s otification where data not found
        Console.WriteLine("Deregister Error:"+itemName+" not found.");
    }
       
}

public bool isValidJSON(String content)
{
    //find pairof json openig&closing - split the string into an array using the character as a delimiter
    if(content.Split('{').Length == content.Split('}').Length && content.Split('[').Length == content.Split(']').Length)
    {   //return true if string is valid JSON
        return true;
    }
    else
    {   //return false if string is not valid JSON
        return false;
    }
    
}

public bool isValidXML(String content)
{
    
     //find pairof json openig&closing - split the string into an array using the character as a delimiter
    if(content.Split('<').Length == content.Split('>').Length)
    {   //return true if string is valid XML
        return true;
    }
    else
    {   //return false if string is not valid XML
        return false;
    }
}

}//end of class

}//end of namespace
