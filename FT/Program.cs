﻿using System;
using System.Collections;
using System.IO;

namespace FT
{
    class Program
    { // class program
        static void Main(string[] args)
        {    //example
            Formulatrix fxObj = new Formulatrix();
            fxObj.Register("tes.json","{data:achmad}}",1);
            fxObj.Register("tes2.json","{data:alfian}",1);
            fxObj.Register("tes3.xml","<info><data>this is very simple apps</data></info>",2);
            fxObj.Register("tes4.xml","<data>>this error is horrible</data>",2);
            Console.WriteLine("getType of tes.json = "+fxObj.GetType("tes.json"));
             // return 0 means not found
            Console.WriteLine("getType of tes3.xml = "+fxObj.GetType("tes3.xml"));

            fxObj.Retrieve("tes.json");
            fxObj.Retrieve("tes2.json");
            fxObj.Retrieve("tes3.xml");
            fxObj.Retrieve("tes4.xml");
            fxObj.Deregister("tes3.xml");
            fxObj.Retrieve("tes3.xml");
            fxObj.Deregister("tes2.json");
            fxObj.Deregister("tes.json");
            fxObj.GetType("tes.json");
        }   
    }
}